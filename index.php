<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<style>
  .container {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 34vh;
    margin-top: 2rem;
  }

  .form-group {
    width: 30vw;
    height: inherit;
    background-color: white;
    padding: 2rem;
    margin-top: 2rem;
    display: flex;
    flex-direction: column;
    border: 2px solid #4f85b4;
  }

  .form-child {
    height: 36px;
    margin: 10px 0px;
    display: flex;
    align-items: center;
    justify-content: space-around;
    margin: 1.4rem 0px;
  }

  .form-child-text {
    background-color: #5a9bd5;
    height: inherit;
    width: 100px;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0px 6px;
    border: 2px solid #4f85b4;
    color: white;
  }

  .form-input-text {
    height: inherit;
    width: 200px;
    padding: 0px;
    border: 2px solid #4f85b4;
  }

  .form-input-radio {
    display: flex;
    align-items: center;
    height: inherit;
    width: 200px;
    padding: 0px;
  }

  .form-input-select {
    display: flex;
    align-items: center;
    height: inherit;
    width: 200px;
    padding: 0px
  }

  .form-child-btn {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 2rem;
  }

  .btn-submit {
    height: 44px;
    width: 140px;
    border-radius: 5px;
    border: 2px solid #4f85b4;
    background-color: #70ad46;
    color: white;
  }
</style>

<body>
  <div class="container">
    <div class="form-group">
      <form action="" method=" POST">
        <div class="form-child">
          <label class="form-child-text">Họ và tên</label>
          <input type="email" name="email" class="form-input-text" >
        </div>

        <div class="form-child">
          <label class="form-child-text">Giới tính</label>
          <div class="form-input-radio">
            <?php
            $gender = array('0' => 'Nam', '1' => 'Nữ');
            for ($i = 0; $i < count($gender); $i++) {
              echo '
                <input type="radio" id="' . $i . '" name="gender" value="' . $i . '">
                ';
              echo '
                <label for="' . $i . '" style="margin-right: 10px">' . $gender[$i] . '</label> 
                ';
            }
            ?>
          </div>
        </div>

        <div class="form-child">
          <label class="form-child-text">Phân khoa</label>
          <div class="form-input-select">
            <select id="hello" style="height: inherit">
              <?php
              $khoa = array('null' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
              foreach ( $khoa as $key => $value) {
                echo '<option value="' . $key . '">' . $value . '</option>';
              }
              ?>
            </select>
          </div>
        </div>

        <div class="form-child-btn">
          <input type="submit" name="btn" class="btn-submit" value="Đăng ký">
        </div>
      </form>
    </div>
  </div>
</body>

</html>